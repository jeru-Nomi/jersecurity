﻿using JerSDK.Security;
using JerSDK.Security.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace JerSecurityApi.Controllers
{
    public class UserController : ApiController
    {
        [HttpPost]
        [Route("api/User/Insert")]
        public IHttpActionResult InsertUser([FromBody] User User)
        {
            try
            {
                UseFunctions.InsertUser(User);
                return Ok(new { });
            }
            catch (Exception ex)
            {
                return null;
            }

        }
    }
}