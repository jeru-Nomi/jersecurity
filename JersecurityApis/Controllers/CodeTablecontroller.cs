﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace JerSecurityApi.Controllers
{
    public class CodeTableController : ApiController
    {
        [HttpGet]
        [Route("api/CodeTable/GetTeams/")]
        public IHttpActionResult GetTeams()
        {
            try
            {
                return Ok(JerSDK.Security.CodeFunction.GetTeams());
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        [HttpGet]
        [Route("api/CodeTable/GetAreas/")]
        public IHttpActionResult GetAreas()
        {
            try
            {
                return Ok(JerSDK.Security.CodeFunction.GetAreas());
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        [HttpGet]
        [Route("api/CodeTable/GetRoles/{ApplicationModuleID}")]
        public IHttpActionResult GetRoles(int ApplicationModuleID)
        {
            try
            {
                return Ok(JerSDK.Security.CodeFunction.GetRoles(ApplicationModuleID));
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}