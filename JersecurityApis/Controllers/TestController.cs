﻿using log4net.Appender;
using log4net.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace JerSecurityApi.Controllers
{
    public class TestController : ApiController
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private static readonly log4net.ILog log1 = log4net.LogManager.GetLogger("JerSecLgr");

        [HttpGet]
        [Route("api/Test/")]
        public IHttpActionResult Get()
        {
            
            try
            {
                log.Info("Logging the text for " + User.Identity.Name);
                log1.Info("Logging the text for " + User.Identity.Name);
                return Ok(User.Identity.Name);
            }
            catch (Exception ex)
            {
                log.Error("Could not perform the test", ex);
                log1.Error("Could not perform the test", ex);
                return null;
            }
        }

        [HttpGet]
        [Route("api/test/db/")]
        public IHttpActionResult GetTest()
        {
            try
            {
                return Ok(JerSDK.Security.CodeFunction.GetAreas());
            }
            catch (Exception ex)
            {
                log.Error("Could not connect to db", ex);
                log1.Error("Could not connect to db", ex);
                return null;
            }
        }

    }
}
