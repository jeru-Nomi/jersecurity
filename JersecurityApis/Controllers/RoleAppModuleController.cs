﻿
using JerSDK.Security.Models;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http;

namespace JerSecurityApi.Controllers
{
  
    public class RoleAppModuleController : ApiController
    {
        [HttpPost]
        [Route("api/RoleAppModule/Get")]
        public IHttpActionResult GetRoleAppModule([FromBody]JerSDK.Security.Models.RoleAppModuleFilter RoleAppModuleController)
        {
            try
            {
                return Ok(JerSDK.Security.UseFunctions.GetRoleAppModule(RoleAppModuleController));
            }
            catch (Exception ex)
            {
                return null;
            }
        }



        [HttpPost]
        [Route("api/RoleAppModule/Insert")]
        public IHttpActionResult InsertRoleAppModule([FromBody] RoleAppModule RoleAppModule)
        {
            try
            {
                JerSDK.Security.UseFunctions.InsertRoleAppModule(RoleAppModule);
                return Ok(new {});
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        [HttpPost]
        [Route("api/RoleAppModule/Update")]
        public IHttpActionResult UpdateRoleAppModule([FromBody] RoleAppModule RoleAppModule)
        {
            try
            {
                
               JerSDK.Security.UseFunctions.UpdateRoleAppModule(RoleAppModule);
                return Ok(new { });
            }
            catch (Exception ex)
            {
                return null;
            }
        }




    }
}