﻿//using JerNamespace.Security.Models;
using JerNamespace.Security.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http;


namespace JerSecurityApi.Controllers
{
    public class AreaTeamController : ApiController
    {
        [HttpGet]
        [Route("api/AreaTeam/Get/")]
        public IHttpActionResult GetAreaTeam()
        {
            try
            {
                
                return Ok(JerSDK.Security.RoleFunctions.GetAreaTeam());
            }
            catch (Exception ex)
            {
                return null;
            }
        }


        [HttpGet]
        [Route("api/AreaTeamTest")]
        public IHttpActionResult GetAreaTeamTest()
        {
            try
            {
                //string s = "ff";
                //return Ok(new { s});
                List<AreaTeam> hh = JerSDK.Security.RoleFunctions.GetAreaTeam();
                //throw new Exception();
                return Ok(new { hh });
            }
            catch (Exception ex)
            {

                string filePath = @"I:\WWW-12\Jer-Security-test\Error.txt";

                using (StreamWriter writer = new StreamWriter(filePath, true))
                {
                    writer.WriteLine("Message :" + ex.Message + "<br/>" + Environment.NewLine + "StackTrace :" + ex.StackTrace +
                       "" + Environment.NewLine + "Date :" + DateTime.Now.ToString());
                    writer.WriteLine(Environment.NewLine + "-----------------------------------------------------------------------------" + Environment.NewLine);
                }

                return null;
            }
        }
    }
}